# CJBornak717's .bashrc

## Export
#export EDITOR="nano"

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases
alias sudo="sudo "
alias ls="exa --color=always --grid --group-directories-first --modified -ls"
alias c="clear"
alias ping="ping -c 5"
alias wget="wget -c"

# User specific functions

# PowerLine Prompt - As instructed by Fedora Magazine
if [ -f `which powerline-daemon` ]; then
  powerline-daemon -q
  POWERLINE_BASH_CONTINUATION=1
  POWERLINE_BASH_SELECT=1
  . /usr/share/powerline/bash/powerline.sh
fi

# Bash Insulter - From Derek Taylor
if [ -f /etc/bash.command-not-found ]; then
    . /etc/bash.command-not-found
fi
